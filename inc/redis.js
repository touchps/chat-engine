let redis = require('redis'),
    pub = redis.createClient(),
    sub = redis.createClient(),
    persister = redis.createClient();

exports.pub = pub;
exports.sub = sub;
exports.persister = persister;
