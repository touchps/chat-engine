let express = require('express'),
    app = express(),
    server = require('http').createServer(app);,
    bodyParser = require('body-parser'),
    io = require('socket.io')(server),
    path = require('path'),
    redis = require('./inc/redis');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


// static files folder setup
app.use(express.static(path.join(__dirname, 'public')));

// Middleware to parse body request
app.use(bodyParser.urlencoded({
   extended: true
}));

var chat_history = [];
var active_users = [];

// fetch chat history from redis
function fetchMessages(){
   redis.persister.get('chat_history', function(err, reply){
      if(reply) { 
         chat_history = JSON.parse(reply);
      }
   });
}

// fetch active users from redis
function fetchUsers(){
   redis.persister.get('active_users', function(err, reply){
      if(reply) {
         active_users = JSON.parse(reply);
      }
   });
}

fetchMessages();
fetchUsers();

// subscribe to channels
redis.sub.subscribe('chat_messages');
redis.sub.subscribe('active_users');

// listen for messages and emit specific event
redis.sub.on('message', function(channel, message) {
   if (channel == 'chat_messages') {
      io.sockets.emit('message', JSON.parse(message));
   }

   if (channel == 'active_users') {
      io.sockets.emit('users', JSON.parse(message));
   }
});

// handling routes
app.get('/', function(req, res) {
   res.render('index', {title: 'Chat Room'});
});

// chat room route
app.get('/chat/:username', function(req, res) {
   res.render('room', {user: req.params.username});
});

// get message history
app.get('/messages', function(req, res) {
   fetchMessages();
   res.send(chat_history);
});
